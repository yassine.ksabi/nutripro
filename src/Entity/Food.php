<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\FoodRepository;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass=FoodRepository::class)
 */
class Food {
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $category;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $protein;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $carbs;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $fat;

    public function getId(): ?int {
        return $this->id;
    }

    public function getName(): ?string {
        return $this->name;
    }

    public function setName(string $name): self {
        $this->name = $name;

        return $this;
    }

    public function getCategory(): ?string {
        return $this->category;
    }

    public function setCategory(?string $category): self {
        $this->category = $category;

        return $this;
    }

    public function getProtein(): ?float {
        return $this->protein;
    }

    public function setProtein(?float $protein): self {
        $this->protein = $protein;

        return $this;
    }

    public function getCarbs(): ?float {
        return $this->carbs;
    }

    public function setCarbs(?float $carbs): self {
        $this->carbs = $carbs;

        return $this;
    }

    public function getFat(): ?float {
        return $this->fat;
    }

    public function setFat(?float $fat): self {
        $this->fat = $fat;

        return $this;
    }
}
