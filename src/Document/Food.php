<?php
namespace App\Document;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource
 *
 * @ODM\Document
 */
class Food {
    /**
     * @ODM\Id(strategy="INCREMENT", type="integer")
     */
    private $id;

    /**
     * @var string
     * @ODM\Field(type="string")
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @var float
     * @ODM\Field(type="float")
     * @Assert\NotBlank
     * @Assert\Range(min=0, minMessage="The price must be superior to 0.")
     * @Assert\Type(type="float")
     */
    public $protein;

    /**
     * @var float
     * @ODM\Field(type="float")
     * @Assert\NotBlank
     * @Assert\Range(min=0, minMessage="The price must be superior to 0.")
     * @Assert\Type(type="float")
     */
    public $fat;

    /**
     * @var float
     * @ODM\Field(type="float")
     * @Assert\NotBlank
     * @Assert\Type(type="float")
     */
    public $carbs;

    /**
     * @var float
     * @ODM\Field(type="float")
     * @Assert\NotBlank
     * @Assert\Type(type="float")
     */
    public $calories;

    public function getId(): ?int {
        return $this->id;
    }

    public function getProtein(): float {
        return $this->protein;
    }

    public function setProtein(float $protein) {
        $this->protein = $protein;
        return $this;
    }

    public function getFat(): float {
        return $this->fat;
    }

    public function setFat(float $fat) {
        $this->fat = $fat;
        return $this;
    }

    public function getCarbs(): float {
        return $this->carbs;
    }

    public function setCarbs(float $carbs) {
        $this->carbs = $carbs;
        return $this;
    }

    public function getCalories(): float {
        return $this->calories;
    }

    public function setCalories(float $calories) {
        $this->calories = $calories;
        return $this;
    }
}